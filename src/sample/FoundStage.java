package sample;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import static sample.Main.messages;
import static sample.MyFileVisitor.getTwins;

public class FoundStage {
    public static Scene display(){



        //Header
        Label header = new Label(messages.getString("select"));
        header.setFont(Font.font(18));

        //Make ToggleButtons
        ArrayList<HBox> selectColumns = new ArrayList<HBox>();
        for (Path path1 : getTwins().keySet()){
            HBox selectLine = new HBox();
            ToggleButton firstFile = new ToggleButton(path1.toString());
            firstFile.setUserData(path1.toString());
            firstFile.setMaxSize(350,25);
            firstFile.setMinSize(350,25);
            firstFile.selectedProperty().setValue(true);
            ToggleButton secondFile = new ToggleButton(getTwins().get(path1).toString());
            secondFile.setUserData(getTwins().get(path1).toString());
            secondFile.setMaxSize(350,25);
            secondFile.setMinSize(350,25);
            ToggleGroup group = new ToggleGroup();
            group.getToggles().addAll(firstFile, secondFile);
            selectLine.getChildren().addAll(firstFile, secondFile);
            selectLine.setAlignment(Pos.CENTER);
            selectLine.paddingProperty().setValue(new Insets(0,20,0,20));
            selectColumns.add(selectLine);
        }


        //KillButton
        Button killBut = new Button(messages.getString("kill"));
        killBut.setFont(Font.font(18));
        killBut.onActionProperty().setValue(event -> {
            int delFilesQuantity = 0;
            long delFilesSize = 0;
            for (HBox line : selectColumns){
                Toggle selected = ((ToggleButton) line.getChildren().get(0)).getToggleGroup().getSelectedToggle();
                Path path = Paths.get(selected.getUserData().toString());
                if (selected != null && Files.exists(path)){
                    try {
                        delFilesQuantity++;
                        delFilesSize+=Files.size(path);
                        Files.delete(path);
                    } catch (IOException e) {
                        System.out.println(messages.getString("file")+Paths.get(selected.getUserData().toString())+messages.getString("cant"));
                    }
                }
            }
            Stage thisStage = (Stage) killBut.getScene().getWindow();
            thisStage.setScene(setFinallayer(delFilesQuantity,delFilesSize));
        });

        //Make layout
        VBox layout = new VBox();
        layout.getChildren().addAll(header);
        layout.getChildren().addAll(selectColumns);
        layout.getChildren().addAll(killBut);
        layout.setAlignment(Pos.CENTER);
        ScrollPane scrollLayout = new ScrollPane();
        scrollLayout.setContent(layout);

        return new Scene(scrollLayout, 740, 610);
    }

    // Final scene
    private static Scene setFinallayer(int delFilesQuantity, long delFilesSize){
        StackPane finalLayout = new StackPane();
        Label label = new Label(delFilesQuantity + " " + messages.getString("killed") + delFilesSize/1024/1024 + " " + messages.getString("cleaned"));
        label.setFont(Font.font(22));
        finalLayout.getChildren().add(label);
        finalLayout.setAlignment(Pos.CENTER);
        return new Scene(finalLayout, 500, 300);
    }
}
