package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {

    private static TextField dirField;

    //Locale
    static Locale locale = Locale.getDefault();
    //static Locale locale = Locale.forLanguageTag("ru_RU");
    static ResourceBundle messages= ResourceBundle.getBundle("MessagesBundle", locale);


    @Override
    public void start(Stage primaryStage) throws Exception{

        //Menu
        Menu info = new Menu(messages.getString("info"));
        info.getItems().add(new MenuItem(messages.getString("tosite")));
        info.getItems().add(new MenuItem(messages.getString("about")));
        Menu language = new Menu(messages.getString("language"));
        language.getItems().add(new MenuItem(messages.getString("rus")));
        language.getItems().add(new MenuItem(messages.getString("en")));
        MenuBar bar = new MenuBar();
        bar.getMenus().addAll(info, language);

        //Header
        Label askEnter = new Label(messages.getString("enterFolder"));
        askEnter.setFont(Font.font(17));

        //Input directory
        dirField = new TextField();
        dirField.setMinWidth(400);
        dirField.setFont(Font.font(17));

        Button dirBut = new Button("...");
        dirBut.setFont(Font.font(17));
        dirBut.setOnAction(event -> {
            DirectoryChooser dirs = new DirectoryChooser();
            File selectedDir = dirs.showDialog(primaryStage);
            if (selectedDir != null) {
                dirField.setText(selectedDir.getPath());
            }
        });


        //Wait scene
        VBox layout = new VBox();
        Label label = new Label(messages.getString("wait"));
        label.setFont(Font.font(null, FontWeight.BOLD, 36));
        //label.setEffect(new GaussianBlur());
        //ImageView image =new ImageView(new Image("Alex.png"));
        layout.getChildren().add(label);
        //layout.getChildren().add(image);
        layout.setAlignment(Pos.CENTER);
        Scene waitScene = new Scene(layout, 500, 500);



        //GO button
        Button go = new Button(messages.getString("go"));
        go.setFont(Font.font(17));
        go.setOnAction(event -> {
            primaryStage.setScene(waitScene);
            try {
                Files.walkFileTree(Paths.get(dirField.getText()), new MyFileVisitor());
            } catch (IOException e) {
                e.printStackTrace();
            }
            primaryStage.setScene(FoundStage.display());
        });





        //pane, scene, stage
        VBox pane = new VBox();
//        BorderPane pane1 = new BorderPane();
//        pane1.setTop(bar);
//        mainPane.getChildren().add(pane1);
        pane.setSpacing(50);
        HBox block = new HBox();
        block.setAlignment(Pos.CENTER);
        block.getChildren().addAll(dirField, dirBut);
        pane.setAlignment(Pos.CENTER);
        pane.getChildren().addAll( askEnter, block, go);
        primaryStage.setTitle("TwinKiller");
        primaryStage.setScene(new Scene(pane, 500, 300));
        primaryStage.show();

    }




    public static void main(String[] args) {
        launch(args);
    }
}
