package sample;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class MyFileVisitor extends SimpleFileVisitor<Path> {
    private static HashMap<Path, Path> twins = new LinkedHashMap<>();
    private LinkedHashMap<Path, Long> filesNsizes = new LinkedHashMap<>();
    {
        filesNsizes.put(Paths.get(""),-1L);
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        long size = attrs.size();
        for (Path mapFile : filesNsizes.keySet()){
            if (filesNsizes.get(mapFile) == size && isFileSame(mapFile, file)){
                twins.put(mapFile, file);
            }
        }
        if (Files.exists(file)){
            filesNsizes.put(file,size);
        }
        return FileVisitResult.CONTINUE;
    }


    private boolean isFileSame(Path file1, Path file2){
        try(InputStream stream1 = Files.newInputStream(file1); InputStream stream2 = Files.newInputStream(file2)) {
            while (stream1.available()>0){
                if (stream1.read() != stream2.read()){
                    return false;
                }
            }
        } catch (IOException ex){
            ex.printStackTrace();
        }
        return  true;
    }

    public static HashMap<Path, Path> getTwins() {
        return twins;
    }
}
